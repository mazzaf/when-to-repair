//
//  Bookings.swift
//  iRepair
//
//  Created by Federico Mazza on 01/07/16.
//  Copyright © 2016 Federico Mazza. All rights reserved.
//

import Foundation
import RealmSwift

class Booking: Object {
    
    dynamic var item: RepairItem?
    dynamic var user: User?
    dynamic var date: String?
    dynamic var completed:Bool = false
    
    class func clearCompletedBookings() {
        
        let realm = try! Realm()
        let bookings = realm.objects(Booking)
        for i in 0 ..< bookings.count {
            if bookings[i].completed {
                realm.delete(bookings[i])
            }
        }
    }
}