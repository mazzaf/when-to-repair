//
//  ViewController.swift
//  iRepair
//
//  Created by Federico Mazza on 13/05/16.
//  Copyright © 2016 Federico Mazza. All rights reserved.
//

import UIKit
import RealmSwift

class LoginViewController: UIViewController {

    @IBOutlet weak var UsernameTextField: UITextField!
    @IBOutlet weak var PasswordTextField: UITextField!
    
    var user = User()
    
    // MARK: IBActions
    
    @IBAction func loginAction(sender: AnyObject) {

        self.view.endEditing(true)
        performLogin(UsernameTextField.text!, password: PasswordTextField.text!)
    }
    
    
    // MARK: Methods
    
    func performLogin(username: String, password: String) {
        
        let realm = try! Realm()
        
        let users = realm.objects(User)
        
        for i in 0..<users.count {
            
            if users[i].username == username &&
                users[i].password == password {
                
                user = users[i]
                
                User.loggedUser = user
                
                performSegueWithIdentifier("loginSegue", sender: self)
                return
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hideKeyboardWhenTappedAround()
        // Do any additional setup after loading the view, typically from a nib.
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if (segue.identifier == "loginSegue") {
            
            _ = segue.destinationViewController as! PhoneTableViewController
            
            
        
        }
        
    }


}

