//
//  Colours.swift
//  iRepair
//
//  Created by Federico Mazza on 13/06/16.
//  Copyright © 2016 Federico Mazza. All rights reserved.
//

import Foundation


enum Colours: Int {
    
    case white = 0
    case spaceGray = 1
    case gold = 2
    case roseGold = 3
    case black = 4
    case silver = 5
    
}