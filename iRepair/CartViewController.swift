//
//  CartViewController.swift
//  iRepair
//
//  Created by Roberto Cabassi on 03/07/16.
//  Copyright © 2016 Federico Mazza. All rights reserved.
//

import UIKit
import RealmSwift


class CartViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    @IBOutlet weak var totalPriceLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var bookings: Results<Booking>!
    
    // MARK: IBActions
    

    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let realm = try! Realm()
        
        bookings = realm.objects(Booking)//.filter("username == \(User.loggedUser?.username)")
        
        
        print(bookings)
        
        refreshTotal()
        
        
    }
    
    
    func refreshTotal() {
        
        var total = 0.0
        
        for booking in bookings {
            
            if let item = booking.item {
                
                total += item.price
                
            }
            
        }
        
        totalPriceLabel.text = "€ \(total)"
        
    }
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return bookings.count
    }
    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
    
        
        let cell = tableView.dequeueReusableCellWithIdentifier("cartCell", forIndexPath: indexPath) as! CartTableViewCell
        
        let booking = bookings[indexPath.row]
        
        if let item = booking.item, phone = item.phone.first {
            
            cell.itemLabel.text = "\(phone.name) - \(item.name)"
            
            cell.priceLabel.text = "Prezzo: € \(item.price)"
            
        }
        
        
        return cell
    }
    
    
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        let deleteAction = UITableViewRowAction(style: .Destructive, title: "Remove", handler: { (action, indexPath) in
            
            
            let realm = try! Realm()
            
            let booking = self.bookings[indexPath.row]
            
            try! realm.write {
                
                realm.delete(booking)
            }
            
            
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
            
            self.refreshTotal()
            
            print("booking removed")
        })
        
        return[deleteAction]
    }
    
}
