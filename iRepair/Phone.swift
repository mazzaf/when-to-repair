//
//  Phones.swift
//  iRepair
//
//  Created by Federico Mazza on 19/05/16.
//  Copyright © 2016 Federico Mazza. All rights reserved.
//

import Foundation
import RealmSwift

class Phone: Object{
    
    dynamic var name: String = ""
    dynamic var image: NSData?
    dynamic var brand: String = ""
    dynamic var color: Int = 0
    
    let repairItems = List<RepairItem>()
    
}





