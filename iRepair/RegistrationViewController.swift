//
//  RegistrationViewController.swift
//  iRepair
//
//  Created by Federico Mazza on 14/05/16.
//  Copyright © 2016 Federico Mazza. All rights reserved.
//

import UIKit
import RealmSwift

class RegistrationViewController: UIViewController {
    
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var mail: UITextField!
    @IBOutlet weak var button: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        button.layer.cornerRadius = 6
        
        hideKeyboardWhenTappedAround()
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func submit(sender: AnyObject) {
        
        let realm = try! Realm()
        let user = User()
        if let usernameText = username.text,
                passwordText = password.text,
                mailText = mail.text where passwordText.characters.count >= 8 {
            
                user.username = usernameText
                user.password = passwordText
                user.mail = mailText
            
            print("No name problem")
        }
        else
        {
            print("Error in insert")
            return
        }
        
        if User.checkPresence(user) {
            print("user already present")
        }
        else
        {
            try! realm.write{
                realm.add(user)
            }
            print("user added")
        }
        
        
    }
    
}


