//
//  RepairItemTableViewController.swift
//  iRepair
//
//  Created by Federico Mazza on 16/05/16.
//  Copyright © 2016 Federico Mazza. All rights reserved.
//

import UIKit
import RealmSwift

class PhoneTableViewController: UITableViewController {
    
    
    //MARK: Properties
    let reusableCellIdentifier = "PhoneCell"
    
    
    var phones: Results<Phone>!
    
    override func viewDidLoad() {
        
        super.viewDidLoad()        
        let realm = try! Realm()
        phones = realm.objects(Phone)
        
    }
        
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
 
    
    
    
    // MARK: - Table view data source
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return phones.count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
       
        
        let cell = tableView.dequeueReusableCellWithIdentifier(reusableCellIdentifier, forIndexPath: indexPath) as! PhoneTableViewCell
      
        let phone = phones[indexPath.row]
       
        cell.name.text = phone.name
        
        cell.brand.text = phone.brand
        
        if let image = phone.image {
            let uimage = UIImage(data:image, scale:1.0)
            cell.photo.image = uimage

        }
 
        return cell
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "selectItem" {
            let indexPath: NSIndexPath = self.tableView.indexPathForSelectedRow!
            
            let vc = segue.destinationViewController as! RepairItemTableViewController
            
            let passPhone: Phone = phones[indexPath.row]
            
            vc.phone = passPhone

        }
       
    }
    
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
     if editingStyle == .Delete {
     // Delete the row from the data source
     tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: .Fade)
     } else if editingStyle == .Insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(tableView: UITableView, moveRowAtIndexPath fromIndexPath: NSIndexPath, toIndexPath: NSIndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(tableView: UITableView, canMoveRowAtIndexPath indexPath: NSIndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}