//
//  RepairItemTableViewController.swift
//  iRepair
//
//  Created by Federico Mazza on 04/06/16.
//  Copyright © 2016 Federico Mazza. All rights reserved.
//

import UIKit
import RealmSwift

class RepairItemTableViewController: UIViewController, UITableViewDataSource, UITableViewDelegate
{
    
    @IBOutlet weak var phoneImage: UIImageView!
    @IBOutlet weak var phoneName: UILabel!
    @IBOutlet weak var phoneBrand: UILabel!
    @IBOutlet weak var tableView: UITableView!
    
    var phone: Phone!

    
    let reusableCellIdentifier = "ItemCell"
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        tableView.delegate = self
        tableView.dataSource = self
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(true)
        
        phoneImage.image = UIImage(data: phone.image!, scale: 1.0)
        phoneName.text = phone.name
        phoneBrand.text = phone.brand
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return phone.repairItems.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier(reusableCellIdentifier, forIndexPath:indexPath) as! RepairItemTableViewCell

        let item = phone.repairItems[indexPath.row]
        
        
        cell.name?.text = item.name
        

        cell.price?.text = "Prezzo: € \(item.price)"
        
        return cell
    }
   
    func tableView(tableView: UITableView, editActionsForRowAtIndexPath indexPath: NSIndexPath) -> [UITableViewRowAction]? {
        
        let addAction = UITableViewRowAction(style: .Default, title: "Add to cart", handler: { (action, indexPath) in
            
            let todaysDate:NSDate = NSDate()
            let dateFormatter:NSDateFormatter = NSDateFormatter()
            dateFormatter.dateFormat = "dd/mm/yy HH:mm:ss"
            let DateInFormat:String = dateFormatter.stringFromDate(todaysDate)
            
            
            let booking: Booking = Booking()
            
            
            
            let realm = try! Realm()
            
            
            try! realm.write {
                
                booking.item = self.phone.repairItems[indexPath.row]
                booking.user = User.loggedUser
                booking.date = DateInFormat
                realm.add(booking)
            }
            
            print(realm.objects(Booking))
            print("booking added")
        })
        addAction.backgroundColor = UIColor.blueColor()
        return[addAction]
    }
    
        
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */
    
    

}
