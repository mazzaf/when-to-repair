//
//  Users.swift
//  iRepair
//
//  Created by Federico Mazza on 24/06/16.
//  Copyright © 2016 Federico Mazza. All rights reserved.
//

import Foundation
import RealmSwift

class User: Object{
    
    static var loggedUser: User?
    
    dynamic var username: String = ""
    dynamic var password: String = ""
    dynamic var mail: String = ""
    
    class func checkPresence(user: User)-> Bool {
        
        let realm = try! Realm()
        let users = realm.objects(User)
        for i in 0 ..< users.count {
            
            if users[i].mail == user.mail ||
                users[i].username == user.username {
                
                return true
            }
        }
        
        return false
    }

    
}


