//
//  KeyboardResignerExtension.swift
//  iRepair
//
//  Created by Roberto Cabassi on 03/07/16.
//  Copyright © 2016 Federico Mazza. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    func dismissKeyboard() {
        view.endEditing(true)
    }
}
