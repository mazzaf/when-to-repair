//
//  AppDelegate.swift
//  iRepair
//
//  Created by Federico Mazza on 13/05/16.
//  Copyright © 2016 Federico Mazza. All rights reserved.
//

import UIKit
import RealmSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    
    func addMinchia() {
        
        
        let realm = try! Realm()
        
        try! realm.write {
            
            realm.delete(realm.objects(RepairItem))
            realm.delete(realm.objects(Phone))
        }
        
        
        Booking.clearCompletedBookings()
        
        //MARK: load all phones
        
        /*let superUser = Users()
         superUser.username = "federico"
         superUser.password = "mazza"
         superUser.mail = "federico.mazza97@hotmail.it"
         */
        
        let photo1 = UIImage(named: "phone1")!
        let phone1 = Phone()
        phone1.name = "iPhone5s"
        phone1.brand = "Apple"
        phone1.color = Colours.black.rawValue
        phone1.image = UIImagePNGRepresentation(photo1)
        
        let photo2 = UIImage(named: "phone2")!
        let phone2 = Phone()
        phone2.name = "GalaxyS7"
        phone2.brand = "Samsung"
        phone2.color = Colours.roseGold.rawValue
        phone2.image = UIImagePNGRepresentation(photo2)
        
        let photo3 = UIImage(named: "phone3")!
        let phone3 = Phone()
        phone3.name = "Nexus6"
        phone3.brand = "Google"
        phone3.color = Colours.spaceGray.rawValue
        phone3.image = UIImagePNGRepresentation(photo3)
        
        
        try! realm.write {
            
            realm.add(phone1)
            realm.add(phone2)
            realm.add(phone3)
            
        }
        
        //MARK: load all items
        
        let item1 = RepairItem()
        item1.name = "Home button"
        item1.price = 20.00
        
        let item2 = RepairItem()
        item2.name = "Schermo"
        item2.price = 22.00
        
        let item3 = RepairItem()
        item3.name = "Pile a carbone"
        item3.price = 1.23
        
        try! realm.write {
            
            phone1.repairItems.append(item1)
            phone2.repairItems.append(item2)
            phone3.repairItems.append(item3)
        }

    }

    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        // Override point for customization after application launch.
        print(Realm.Configuration.defaultConfiguration.fileURL)
        
        
        //addMinchia()
        
        return true
    }

    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(application: UIApplication) {
        // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

