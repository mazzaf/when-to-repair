//
//  PhoneTableViewCell.swift
//  iRepair
//
//  Created by Federico Mazza on 27/05/16.
//  Copyright © 2016 Federico Mazza. All rights reserved.
//

import UIKit

class PhoneTableViewCell: UITableViewCell{
    @IBOutlet weak var photo: UIImageView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var brand: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
