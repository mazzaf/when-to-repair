//
//  RepairItemTableViewCell.swift
//  iRepair
//
//  Created by Federico Mazza on 28/05/16.
//  Copyright © 2016 Federico Mazza. All rights reserved.
//

import UIKit

class RepairItemTableViewCell: UITableViewCell {
   
    @IBOutlet weak var name: UILabel?
    @IBOutlet weak var price: UILabel?
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override func awakeFromNib() {
        
        super.awakeFromNib()
        // Initialization code
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}

