//
//  RepairItems.swift
//  iRepair
//
//  Created by Federico Mazza on 28/05/16.
//  Copyright © 2016 Federico Mazza. All rights reserved.
//

import Foundation
import RealmSwift

class RepairItem: Object{
    
    dynamic var name: String = ""
    dynamic var price = 0.0
    
    let phone = LinkingObjects(fromType: Phone.self, property: "repairItems")
    
}





